#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from invite import Invite
from protocol import JsonProtocol
from communication import Communication
import sys
import inspect
from file_manager import FileManager
from list_commands import list_commands

class Client:
    def __init__(self, proto, comunication):
        self.proto = proto
        self.comunication = comunication

    def close(self):
        self.communication.close()

    def getInputInJSON(self, input):
        return self.proto.processCommand(input)

    def processServerResponse(self, serverResponse):
        return self.proto.processData(serverResponse)

    def sendToServer(self, data):
        self.comunication.send(data)

    def receiveFromServer(self):
        return self.comunication.receive()

    def checkIfMethod(self, response):
        method = response.split("?", 1)[0]
        return hasattr(self, method) and inspect.ismethod(getattr(self, method))

    def processCommand(self, command):
        try:
            response_JSON = client.getInputInJSON(command)
            client.sendToServer(response_JSON)
            server_response = client.receiveFromServer()
            server_response = client.processServerResponse(server_response)
            return server_response
        except AttributeError:
            print('The command ' + response + ' does not exist.')

    def connecter(self):
        try:
            self.processCommand('salutation')
            return 'oui'
        except:
            return 'non'

    def fichier(self):
        #TODO
        pass

    def dossier(self):
        #TODO
        pass

##############################################################################################
### Main ###
##############################################################################################
if __name__ == '__main__':
    HOST = '159.89.124.41'
    if len(sys.argv) == 1:
        #TODO faire syncronization

        ok = 0
    elif len(sys.argv) == 2:
        PORT = int(sys.argv[1])
        client = Client(JsonProtocol(list_commands, FileManager()), Communication(HOST, PORT))
        invite = Invite()
        response = ""
        while response != "quitter":
            response = invite.getInput()
            if client.checkIfMethod(response):
                method = getattr(client, response.split('?')[0])
                print(method())
            else:
                print(client.processCommand(response))
        client.close()
    elif len(sys.argv) > 2:
        raise Exception('le programme ne peut pas prendre plus que 1 paramètre')
