#!/usr/bin/python3
# -*- coding: utf-8 -*-

from command import Command, NoActionCommand
from abc import ABCMeta, abstractmethod
from file_manager import FileManager
import json
from list_commands import list_commands

class Protocol(object):

    __metaclass__ = ABCMeta

    @abstractmethod
    def processCommand(self, input): pass

    @abstractmethod
    def processData(self, data): pass

class JsonProtocol(Protocol):

    def __init__(self, commands, file_manager):
        self.commands = commands
        self.file_manager = file_manager

    def processCommand(self, input):
        cmd = input.split(" ",1)[0]

        for command in self.commands:
            if command.match(cmd):
                if len(input.split(" ",1)) > 1:
                    command.processArg(input.split(" ",1)[1], self.file_manager)
                return json.dumps(command.data())

    def processData(self, data):
        return json.loads(data)
