import socket, sys


class Communication:

    MAX_RECV = 1024 * 1024 * 512

    def __init__(self, host, port):
        "Connexion du client au serveur"

        # Se donner un objet de la classe socket.
        self.sock = socket.socket()

        # Fixé à non-blocking
        self.sock.settimeout(None)

        # Connexion au serveur
        self.sock.connect((host, port))

    def send(self, data):
        msgClient = data + "\r\n"
        self.sock.send(msgClient.encode(encoding='UTF-8'))

    def receive(self):
        return self.sock.recv(Communication.MAX_RECV).decode('UTF-8')

    def close(self):
        self.sock.close
