#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os, time, hashlib, math

class FileManager:
    def info(self, path):
        if not os.path.exists(path):
            raise Exception('The file ' + path + ' does not exist.')

        hasher = hashlib.sha256()

        with open(path, 'r', encoding='ISO-8859-1') as specified_file:
            file_content = specified_file.read()
            hasher.update(file_content.encode('utf-8'))
            info = {
                    'name': path.split('/')[-1],
                    'folder': path.rsplit('/',1)[0] + '/',
                    'date': os.stat(path).st_mtime,
                    'content': file_content,
                    'signature': hasher.hexdigest() }
            specified_file.close()

            if len(path.rsplit('/',1)) == 1:
                info['folder'] = "./"

        return info

    def createFile(self, file_path, content):
        if os.path.exists(file_path):
            raise Exception('The file ' + file_path + ' already exists.')
        with open(file_path, 'w', encoding='ISO-8859-1') as new_file:
            new_file.write(content)
            new_file.close()

    def createFolder(self, folder_path):
        if os.path.exists(folder_path):
            raise Exception('The folder ' + folder_path + ' already exists.')

        os.makedirs(newpath)
