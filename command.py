#!/usr/bin/python3
# -*- coding: utf-8 -*-

from abc import ABCMeta, abstractmethod
from file_manager import FileManager

class InterfaceCommand(object):

    __metaclass__ = ABCMeta

    @abstractmethod
    def match(self, input): pass

    @abstractmethod
    def json(self): pass

    @abstractmethod
    def processArg(self, argument, file_manager = None):
        pass

#Classe utilisé pour créer une commande de base qui ne prend pas d'argument
class Command(InterfaceCommand):
    def __init__(self, specified_input, specified_output):
        self.specified_input = specified_input
        self.specified_output = specified_output

    def match(self, input):
        return self.specified_input == input

    def data(self):
        return self.specified_output

    def processArg(self, argument, file_manager = None):
        pass

#Classe utilisé dans le cas ou la commande prend un argument et que ce dernier est utilisé tel-quel
class SimpleArgumentCommand(Command):
    def processArg(self, argument, file_manager = None):
        self.specified_output = { list(self.specified_output.keys())[0]: argument }

#Commande televerser?
class UploadFileCommand(Command):
    def processArg(self, argument, file_manager):
        file_info = file_manager.info(argument)

        self.specified_output = {
            list(self.specified_output.keys())[0]: {
                'nom': file_info['name'],
                'dossier': file_info['folder'],
                'signature': file_info['signature'],
                'contenu': file_info['content'],
                'date': file_info['date']
                }
            }

#Commande telecharger?
class DownloadFileCommand(Command):
    def processArg(self, argument, file_manager):
        fichier = argument.rsplit('/', 1)[1]
        dossier = argument.rsplit('/', 1)[0] + "/"
        print(dossier + '+' + fichier)
        self.specified_output = {
            list(self.specified_output.keys())[0]: {
                'nom': fichier,
                'dossier': dossier
                }
            }

#Commande supprimerFichier?
class DeleteFileCommand(Command):
    def processArg(self, argument, file_manager):
        fichier = argument.rsplit('/', 1)[1]
        dossier = argument.rsplit('/', 1)[0] + "/"

        self.specified_output = {
            list(self.specified_output.keys())[0]: {
                'nom': fichier,
                'dossier': dossier
                }
            }

#Commande fichierRecent?
class RecentFileCommand(Command):
    def processArg(self, argument, file_manager):
        file_info = file_manager.info(argument)

        self.specified_output = {
            list(self.specified_output.keys())[0]: {
                'nom': file_info['name'],
                'dossier': file_info['folder'],
                'date': file_info['date']
                }
            }

#Commande fichierIdentique?
class IdenticalFileCommand(Command):
    def processArg(self, argument, file_manager):
        file_info = file_manager.info(argument)

        self.specified_output = {
            list(self.specified_output.keys())[0]: {
                'nom': file_info['name'],
                'dossier': file_info['folder'],
                'signature': file_info['signature'],
                'date': file_info['date']
                }
            }

#Commande retournant un message d'erreur dans le cas ou la commande entrée par l'utilisateur n'existe pas
class NoActionCommand(InterfaceCommand):
    def match(self, input):
        self.input = input
        return True

    def data(self):
        raise AttributeError()
        #return 'The command ' + self.input + ' does not exist.'

    def processArg(self, argument, file_manager): pass
