from command import *

list_commands = [
        Command('salutation', {'salutation': 'bonjourServeur'}),
        Command('nomServeur?', {'questionNomServeur': ''}),
        SimpleArgumentCommand('listeDossier?', {'questionListeDossiers': ''}),
        SimpleArgumentCommand('listeFichier?', {'questionListeFichiers': ''}),
        SimpleArgumentCommand('creerDossier?', {'creerDossier': ''}),
        UploadFileCommand('televerser?', {'televerserFichier': ''}),
        DownloadFileCommand('telecharger?', {'telechargerFichier': ''}),
        SimpleArgumentCommand('supprimerDossier?', {'supprimerDossier': ''}),
        DeleteFileCommand('supprimerFichier?', {'supprimerFichier': ''}),
        IdenticalFileCommand('fichierIdentique?', {'questionFichierIdentique': ''}),
        RecentFileCommand('fichierRecent?', {'questionFichierRecent': ''}),
        Command('miseAjour', {'miseAjour': ''}),
        Command('quitter', {'quitter': ''}),
        NoActionCommand()
    ]
