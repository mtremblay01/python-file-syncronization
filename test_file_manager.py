#!/usr/bin/python3
# -*- coding: utf-8 -*-

from unittest import TestCase
import unittest.mock, os, time, hashlib

from file_manager import FileManager

class TestFileManager(TestCase):
    def setUp(self):
        self.file_manager = FileManager()

        self.file_name = 'python_app_file_manager_test_file.test'
        self.folder = '/tmp/'
        self.file_content = 'Lorem ipsum dolor sit amet...'

        hasher = hashlib.sha256()

        with open(self.folder + self.file_name, 'w', encoding='ISO-8859-1') as test_file:
            self.file_date = os.path.getctime(self.folder + self.file_name)
            test_file.write(self.file_content)
            hasher.update(self.file_content.encode('utf-8'))
            self.file_signature = hasher.hexdigest()
            test_file.close()

    def test_info(self):
        with open(self.folder + self.file_name, 'r', encoding='ISO-8859-1') as test_file:
            info = self.file_manager.info(self.folder + self.file_name)
            test_file.close()

        self.assertEqual(info['name'], self.file_name)
        self.assertEqual(info['folder'], self.folder)
        self.assertEqual(info['date'], self.file_date)
        self.assertEqual(info['content'], self.file_content)
        self.assertEqual(info['signature'], self.file_signature)

    def tearDown(self):
        os.remove(self.folder + self.file_name)

if __name__ == '__main__':
    unittest.main()
