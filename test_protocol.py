#!/usr/bin/python3
# -*- coding: utf-8 -*-

from unittest import TestCase
import unittest.mock
import json
from protocol import JsonProtocol
from command import InterfaceCommand

class TestJsonProtocol(TestCase):
    def setUp(self):
        self.input = 'test_input'
        self.output = {'test key': 'test value'}
        self.mock_cmd = [ MockCommand(self.input, self.output) ]
        self.json_protocol = JsonProtocol(self.mock_cmd, None)

    def test_processCommand(self):
        actual_output = self.json_protocol.processCommand(self.input)
        self.assertEqual(actual_output, json.dumps(self.output))

    def test_processData(self):
        actual_output = self.json_protocol.processData(json.dumps(self.output))
        self.assertEqual(actual_output, self.output)

class MockCommand(InterfaceCommand):

    def __init__(self, input, output):
        self.input = input
        self.output = output

    def match(self, input):
        return True

    def data(self):
        return self.output

if __name__ == '__main__':
    unittest.main()
