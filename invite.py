#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class Invite:

    def __init__(self, prompt=">>> "):
        """Constructeur de l'interpréteur"""
        self.prompt = prompt
        self.commande = ''

    def getInput(self):
        """Une interaction d'entrée/sortie complète"""
        response = input(self.prompt)
        return response