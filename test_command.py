#!/usr/bin/python3
# -*- coding: utf-8 -*-

from unittest import TestCase
import unittest.mock
from command import *

class TestCommand(TestCase):

    def setUp(self):
        self.cmd = Command('test_input', {'test_output': ''})

    def test_match(self):
        self.assertEqual(self.cmd.match('test_input'), True)

    def test_match_with_invalid_cmd(self):
        self.assertEqual(self.cmd.match('wrong input'), False)

    def test_data(self):
        self.assertEqual(self.cmd.data(), {'test_output': ''})

class TestSimpleArgumentCommand(TestCase):

    def setUp(self):
        self.cmd = SimpleArgumentCommand('test_input', {'test_output': ''})

    def test_processArg_process_value(self):
        self.cmd.processArg('d1/d2/f1')
        self.assertEqual(list(self.cmd.data().values())[0], 'd1/d2/f1')

    def test_processArg_process_key(self):
        self.assertEqual(list(self.cmd.specified_output.keys())[0], 'test_output')

class TestUploadFileCommand(TestCase):

    def setUp(self):
        self.cmd = UploadFileCommand('test_input', {'test_output': ''})
        self.file_manager = MockFileManager

    def test_processArg_process_key(self):
        self.assertEqual(list(self.cmd.specified_output.keys())[0], 'test_output')

    def test_processArg_process_value(self):
        self.cmd.processArg('any_value', self.file_manager)
        #print(self.cmd.data()['test_output'])
        self.assertEqual(self.cmd.data()['test_output']['nom'], 'file_name')
        self.assertEqual(self.cmd.data()['test_output']['dossier'], 'folder/')
        self.assertEqual(self.cmd.data()['test_output']['date'], 'file_date')
        self.assertEqual(self.cmd.data()['test_output']['signature'], 'file_signature')
        self.assertEqual(self.cmd.data()['test_output']['contenu'], 'file_content')

class TestDownloadFileCommand(TestCase):

    def setUp(self):
        self.cmd = UploadFileCommand('test_input', {'test_output': ''})
        self.file_manager = MockFileManager

    def test_processArg_process_key(self):
        self.assertEqual(list(self.cmd.specified_output.keys())[0], 'test_output')

    def test_processArg_process_value(self):
        self.cmd.processArg('any_value', self.file_manager)
        #print(self.cmd.data()['test_output'])
        self.assertEqual(self.cmd.data()['test_output']['nom'], 'file_name')
        self.assertEqual(self.cmd.data()['test_output']['dossier'], 'folder/')

class TestDeleteFileCommand(TestCase):

    def setUp(self):
        self.cmd = UploadFileCommand('test_input', {'test_output': ''})
        self.file_manager = MockFileManager

    def test_processArg_process_key(self):
        self.assertEqual(list(self.cmd.specified_output.keys())[0], 'test_output')

    def test_processArg_process_value(self):
        self.cmd.processArg('any_value', self.file_manager)
        #print(self.cmd.data()['test_output'])
        self.assertEqual(self.cmd.data()['test_output']['nom'], 'file_name')
        self.assertEqual(self.cmd.data()['test_output']['dossier'], 'folder/')

class TestRecentFileCommand(TestCase):

    def setUp(self):
        self.cmd = UploadFileCommand('test_input', {'test_output': ''})
        self.file_manager = MockFileManager

    def test_processArg_process_key(self):
        self.assertEqual(list(self.cmd.specified_output.keys())[0], 'test_output')

    def test_processArg_process_value(self):
        self.cmd.processArg('any_value', self.file_manager)
        #print(self.cmd.data()['test_output'])
        self.assertEqual(self.cmd.data()['test_output']['nom'], 'file_name')
        self.assertEqual(self.cmd.data()['test_output']['dossier'], 'folder/')
        self.assertEqual(self.cmd.data()['test_output']['date'], 'file_date')

class TestIdenticalFileCommand(TestCase):

    def setUp(self):
        self.cmd = UploadFileCommand('test_input', {'test_output': ''})
        self.file_manager = MockFileManager

    def test_processArg_process_key(self):
        self.assertEqual(list(self.cmd.specified_output.keys())[0], 'test_output')

    def test_processArg_process_value(self):
        self.cmd.processArg('any_value', self.file_manager)
        #print(self.cmd.data()['test_output'])
        self.assertEqual(self.cmd.data()['test_output']['nom'], 'file_name')
        self.assertEqual(self.cmd.data()['test_output']['dossier'], 'folder/')
        self.assertEqual(self.cmd.data()['test_output']['date'], 'file_date')
        self.assertEqual(self.cmd.data()['test_output']['signature'], 'file_signature')

class MockFileManager:
    def info(path):
        return {
                'name': 'file_name',
                'folder': 'folder/',
                'date': 'file_date',
                'content': 'file_content',
                'signature': 'file_signature' }

if __name__ == '__main__':
    unittest.main()
